import javax.swing.JApplet;
import java.awt.Graphics;
// Textbook - pg 42 - problem 8
public class AssignmentOne extends JApplet {
	public void paint(Graphics canvas) {
		canvas.drawArc(40, -50, 120, 100, 0, -180); //top arc
		canvas.drawArc(-50, 40, 100, 120, 90, -180); //left arc
		canvas.drawArc(150, 40, 100, 120, 90, 180); //right arc
		canvas.drawArc(40, 150, 120, 100, 180, -180); //bottom arc
		canvas.drawOval(50, 50, 100, 100); //center circle
		canvas.fillOval(75, 75, 50, 50);
	}

}
