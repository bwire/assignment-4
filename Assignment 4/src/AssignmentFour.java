
public class AssignmentFour {
	public static void main(String[] args)
	{
		Room roomOne = new Room("yellow","hardwood",1);
		Room roomTwo = new Room("purple","tiled",0);
		Room roomThree = new Room("white","carpeted",3);
		System.out.println("The first room is:");
		roomOne.printRoom();
		System.out.println("The second room is:");
		roomTwo.printRoom();
		System.out.println("The third room is:");
		roomThree.printRoom();
	}
}
