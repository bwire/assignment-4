public class Room {
		private String wallColor;
		private int floor;
		private int windows;
		private final String[] FLOOR_TYPE = {"Hardwood","Carpeted","Tiled"};
		
		public Room(){
			//Default room has black walls, hardwood floors, no windows
			wallColor = "Black";
			floor = 0;
			windows = 0;
		}
		public Room(String initialWallColor, int initialFloor, int initialWindows) {
			wallColor = initialWallColor;
			floor = initialFloor;
			windows = Math.max(0,initialWindows);
		}
		public Room(String initialWallColor, String initialFloor, int initialWindows) {
			//Allows setting of floor type with a string or integer
			wallColor = initialWallColor;
			windows = Math.max(0,initialWindows);
			switch(initialFloor.toLowerCase()) {
			case "hardwood":
				floor = 0;
				break;
			case "carpeting":
			case "carpeted":
				floor = 1;
				break;
			case "tiled":
				floor = 2;
				break;
			default:
				floor = 0;
				break;
			}
		}
		public String getWallColor() {
			return wallColor;
		}
		public void setWallColor(String newColor) {
			wallColor = newColor;
		}
		public String getFloor() {
			return FLOOR_TYPE[floor];
		}
		public void setFloor(int newFloor) {
			floor = newFloor;
		}
		public void setFloor(String newFloor) {
			switch(newFloor.toLowerCase()) {
			case "hardwood":
				floor = 0;
				break;
			case "carpeting":
			case "carpeted":
				floor = 1;
				break;
			case "tiled":
				floor = 2;
				break;
			default:
				floor = 0;
				break;
			}
		}
		public int getWindows() {
			return windows;
		}
		public void setWindows(int newWindows) {
			windows = Math.max(0,newWindows);
		}
		public void printRoom() {
			System.out.print("This room has "+wallColor+" walls, "+
								FLOOR_TYPE[floor]+" floors, and ");
			if(windows == 0) {  //Displaying proper english for window amount
				System.out.println("no windows.");
			}
			else if(windows == 1) {
				System.out.println("1 window.");
			}
			else {
				System.out.println(windows+" windows.");
			}
		}

}
